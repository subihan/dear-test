import syncmysql from 'sync-mysql';
import mysql from 'mysql';
import http from '../util/httpServices';
import async from 'async';
import CONFIG from '../../db_setting';
import {getCurrentTime} from '../util/Utils';

const MAX_TRANS = 60;

function processToWms(channel, start_date, f_cb) {
	const channel_id = channel.id;
	let orderAdded = 0;
  var dbFinanceCon = new syncmysql(CONFIG.finance);
    // get last id of menarini
    let pSql = `SELECT tb_order.id as order_id, cm_id, dear_clientid, order_number, tb_order.created_at, ` +
                  `tb_order.customer_name as customer_name, addr_line, addr_area, addr_district, addr_city, ` + 
									`addr_province, addr_country, addr_postcode, tb_courier.courier_name, tb_order.courier_slug, ` + 
									`sub_total, shipping_cost, order_total, warehouse_name, tb_customer.cust_phone, `+ 
                  `DATE_FORMAT(tb_order.created_at, '%Y-%m-%d %H:%i:%s') as created_at, is_fullfillsaleadded,`+   
                  `DATE_FORMAT(DATE_ADD(tb_order.created_at, INTERVAL 3 DAY), '%Y-%m-%d %H:%i:%s') as ShipBy `+
                `FROM tb_order, tb_channel, tb_customer, tb_address, tb_marketplace, tb_courier ` + 
                `WHERE tb_order.is_valid=1 AND is_fullfillitemadded=0 AND tb_channel.id = tb_order.channel_id AND `+
                  `tb_customer.id=tb_order.customer_shipid AND ` + 
									`tb_address.id=tb_customer.address_id AND tb_order.channel_status<>'canceled' AND ` +
									`tb_marketplace.id=tb_order.marketplace_id AND tb_courier.id=tb_order.courier_id AND ` +
									`tb_order.channel_id=${channel_id} AND tb_order.created_at>='${start_date}' ` +
									`order by tb_order.created_at;`;
		//console.log(pSql);
    let results = dbFinanceCon.query(pSql);
    if (results.length>0){
			//console.log(`TOTAL found ${results.length}`)

			// Add to WMS
			async.eachSeries(results, function(order,cb){
				if (orderAdded<MAX_TRANS){
					// console.log(`Porcess order: ${order.order_number}`)
					let orderLine = [];
					let skuNotFound = 0;
					pSql = `SELECT tb_orderitems.*, product_sku, product_name, wms_productid ` +
												`FROM tb_orderitems, tb_product ` +
												`WHERE tb_orderitems.order_id=${order.order_id} AND tb_product.id=tb_orderitems.product_id;`;
					let myOrder = dbFinanceCon.query(pSql);
					if (myOrder.length>0){
						//console.log(`Porcess order: ${order.order_number}`)
						let total_amount;
						myOrder.map(item=>{
								if (item.wms_productid){
									// check product sku and get new SKU-Name
									if (item.wms_productid.length<36) {
										skuNotFound++;
										pSql = `INSERT INTO tb_log(log_text) VALUES ("wms_order::processToWms:>order id ${order.order_number} product sku: ${item.product_sku} no WMS ID")`;
										let myErr = dbFinanceCon.query(pSql);
									}
									const total_price = item.selling_price*item.item_count;
									let orderItemTmp = {
												ProductID: `${item.wms_productid}`,
												Name: `${item.product_name}`,
												Quantity: item.item_count,
												Price: item.selling_price,
												Discount: 0,
												Tax: 0,
												TaxRule: `Tax on Sales`,
												Total: total_price
									}
									total_amount=total_amount+total_price;
									orderLine.push(orderItemTmp);
								} else {
									console.log(item.wms_productid)
									skuNotFound++;}
						})
						
						// add to dear wms it all items found
						if (skuNotFound===0){
							// console.log(`Porcess order: ${order.order_number}`)

							// get customer id
							let addr_line1 = order.addr_line.substr(0,254);
							let addr_line21 = `${order.addr_area}, ${order.addr_district}`;
							let addr_line2 = addr_line21.substr(0,254);
							const BillingAddress = {
											Line1: `${addr_line1}`,
											Line2: `${addr_line2}`,
											City: `${order.addr_city}`,
											State: `${order.addr_province}`,                      
											Postcode: `${order.addr_postcode}`,
											Country: `${order.addr_country}`
							};

							const ShippingAddress = {
									Line1: `${addr_line1}`,
									Line2: `${addr_line2}`,
									City: `${order.addr_city}`,
									State: `${order.addr_province}`,                      
									Postcode: `${order.addr_postcode}`,
									Country: `${order.addr_country}`,
									Contact: `${order.customer_name}`,
									ShipToOther: false
							};

							const salesData = {
											CustomerID: `${order.dear_clientid}`,
											Phone: `${order.bill_phone}`,
											Email: '',
											Contact: `${order.customer_name}`,
											DefaultAccount: '4000',
											BillingAddress: BillingAddress,
											ShippingAddress: ShippingAddress,
											TaxRule: 'Tax on Sales',
											TaxInclusive: false,
											Terms: '30 days',
											PriceTier: 'Tier 1',
											Location: `${order.warehouse_name}`,
											Note: '',
											CustomerReference: `${order.order_number}`,
											AutoPickPackShipMode: 'NOPICK',
											SalesRepresentative: 'DEFAULT billing contact',
											Carrier: `${order.courier_name}`,
											CurrencyRate: '1',
											ExternalID: `${order.order_id}`,
											Note: `Carrier: ${order.courier_name}, Type: ${order.courier_slug}`,
											AdditionalAttributes: {
												AdditionalAttribute1: "",
												AdditionalAttribute2: "",
												AdditionalAttribute3: "",
												AdditionalAttribute4: "",
												AdditionalAttribute5: "",
												AdditionalAttribute6: "",
												AdditionalAttribute7: "",
												AdditionalAttribute8: "",
												AdditionalAttribute9: "",
												AdditionalAttribute10: ""
											},
											ShipBy: `${order.ShipBy}`,
											SaleOrderDate: `${order.created_at}`,
											SkipQuote: true,
							};

							if (order.is_fullfillsaleadded===1) {
								pSql = `SELECT wms_salesid FROM tb_fullfillmentorders WHERE cm_orderid=${order.cm_id}`;
								let MyOrderID = dbFinanceCon.query(pSql);
								let salesOrderData = {
									SaleID: `${MyOrderID[0].wms_salesid}`,
									Memo: `Carrier: ${order.courier_name}, Type: ${order.courier_slug}`,
									Status: 'DRAFT',
									AutoPickPackShipMode: 'NOPICK',
									Lines: orderLine,
									TotalBeforeTax: total_amount,
									Tax: 0,
									Total: total_amount
								};
								http.post(CONFIG.apiDearUrl+'sale/order', salesOrderData, CONFIG.dearHeaderConf)
								.then(function(response){
									pSql = `UPDATE tb_order SET is_fullfillitemadded=1 WHERE id=${order.order_id}`;
									myRes = dbFinanceCon.query(pSql);
									orderAdded++;
									return cb(null);                    
								})
								.catch(function(err){
									//console.log(err)
									//console.log(`order id ${order.order_number} failed added to wms`)
									pSql = `INSERT INTO tb_log(log_text) VALUES ("wms_order::processToWms:>order id ${order.order_number} failed to create sale/order wms")`
									let myLog1 = dbFinanceCon.query(pSql);
									return cb(null);                    
								});
							} else {
								http.post(CONFIG.apiDearUrl+'sale', salesData, CONFIG.dearHeaderConf)
								.then(function(response){
									const {data} = response;
									pSql = `INSERT INTO tb_fullfillmentorders (channel_id, order_number, created_at, ` +
														`cm_orderid, cm_orderstatus, wms_salesid, is_valid) ` +
													`VALUES (${channel_id}, '${order.order_number}', CURRENT_TIMESTAMP, ` +
															`${order.cm_id}, 'created', '${data.ID}', 1);`;
									// console.log(pSql)	
									let myRes = dbFinanceCon.query(pSql);
									pSql = `UPDATE tb_order SET is_fullfillsaleadded=1 WHERE id=${order.order_id}`;
									myRes = dbFinanceCon.query(pSql);
									let salesOrderData = {
										SaleID: `${data.ID}`,
										Memo: `Carrier: ${order.courier_name}, Type: ${order.courier_slug}`,
										Status: 'DRAFT',
										AutoPickPackShipMode: 'NOPICK',
										Lines: orderLine,
										TotalBeforeTax: total_amount,
										Tax: 0,
										Total: total_amount
									};
									http.post(CONFIG.apiDearUrl+'sale/order', salesOrderData, CONFIG.dearHeaderConf)
									.then(function(response){
										pSql = `UPDATE tb_order SET is_fullfillitemadded=1 WHERE id=${order.order_id}`;
										myRes = dbFinanceCon.query(pSql);
										orderAdded++;
										return cb(null);                    
									})
									.catch(function(err){
										//console.log(err)
										//console.log(`order id ${order.order_number} failed added to wms`)
										pSql = `INSERT INTO tb_log(log_text) VALUES ("wms_order::processToWms:>order id ${order.order_number} failed to create sale/order wms")`
										let myLog1 = dbFinanceCon.query(pSql);
										return cb(null);                    
									});
								})	
								.catch(function(err){
									//console.log(err)
									//console.log(`sale id ${order.order_number} failed added to wms`)
									pSql = `INSERT INTO tb_log(log_text) VALUES ("wms_order::processToWms:>order id ${order.order_number} failed to create sale wms")`
									let myLog2 = dbFinanceCon.query(pSql);
									return cb(null);                    
								});
							}
						} else {cb(null);}
					} else {console.log(`order ${order.order_number} not found`);return cb(null)}
				} else {return cb(null)}
			}, function(e){
				const timestamp = getCurrentTime();
				
				return f_cb(null, {message: `${timestamp} CH: ${channel.channel_name}: ${orderAdded} order added to WMS`})})
		} else {
			const timestamp = getCurrentTime(); 
			return f_cb(null, {message: `${timestamp} CH: ${channel.channel_name} NO new Data Found`})}
} 

export default { processToWms }